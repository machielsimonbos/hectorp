DataFile            TEST2.mom
DataDirectory       ./raw_files
OutputFile          ./obs_files/TEST2.mom
PhysicalUnit        mm
ScaleFactor         1.0
periodicsignals     365.25 182.625
estimateoffsets     yes
NoiseModels         GGM White
GGM_1mphi           6.9e-06
useRMLE             no
