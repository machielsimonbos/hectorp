DataFile            test_data.msf
DataDirectory       pre_files
OutputFile          fin_files/test_data.msf
PhysicalUnit        mGal
ScaleFactor         1.0
NoiseModels         GGM White
#NoiseModels         White
GGM_1mphi           1.0e-05
useRMLE             no
TS_format           msf
ColumnName          accelerometer
PlotName            accelerometer
Verbose             yes
