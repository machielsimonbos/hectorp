SimulationDir           ./obs_files
SimulationLabel         test_base
NumberOfSimulations     10
NumberOfPoints          5000
SamplingPeriod          1.0
TimeNoiseStart          1000
NoiseModels             GGM White
GGM_1mphi               6.9e-06
RepeatableNoise         yes
